fun main() {
    println("What would you like to play?")
    var game = readLine()
    if (game == "monopoly") {
        println("Here your monopoly")
    } else if (game == "tabu") {
        println("Here your tabu")
    } else if (game == "pictureka") {
        println("Here your pictureka")
    } else {
        println("No games at all!")
    }

    println("Do you wanna sleep?")
    var sleep = readLine()
    if (sleep == "yes") {
        println("Go and sleep!")
        println("Do you wanna read a book?")
        var answer = readLine()
        if (answer == "yes") {
            println("Read this book!")
        } else {
            println("Go back to you game!")
        }
    }

    println("Do you wanna ride a bike?")
    var ans2 = readLine()
    if(ans2=="yes"){
        println("Ride your bike!")
    }else{
        println("Go back to your game!")
    }
}